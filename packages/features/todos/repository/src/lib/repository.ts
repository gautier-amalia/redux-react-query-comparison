import superagent from 'superagent';

import { PatchTodoDto, PostTodoDto, Todo } from '@demo-react-query/features/todos/types';

const http = superagent.agent();
const e = encodeURIComponent;

export class TodoRepository {
  public readonly apiBaseUrl = 'http://localhost:3000/api/todos';

  async listAll(): Promise<Todo[]> {
    const { body } = await http.get(this.apiBaseUrl);
    return body;
  }

  async create(postTodoDto: PostTodoDto): Promise<Todo> {
    const { body } = await http
      .post(this.apiBaseUrl)
      .send(postTodoDto);

      return body;
  }

  async updateOne(
    id: string,
    patchTodoDto: PatchTodoDto,
  ): Promise<Todo> {
    const { body } = await http
      .patch(`${this.apiBaseUrl}/${e(id)}`)
      .send(patchTodoDto);

    return body;
  }

  async deleteOne(id: string): Promise<boolean> {
    const { status } = await http
      .delete(`${this.apiBaseUrl}/${e(id)}`);

    return status === 200;
  }
}
