export interface Todo {
  id: string;
  label: string;
  isDone: boolean;
}

export interface PostTodoDto {
  label: string;
  isDone?: boolean;
}

export interface PatchTodoDto {
  label?: string;
  isDone?: boolean;
}
