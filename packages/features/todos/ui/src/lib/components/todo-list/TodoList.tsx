import { memo } from 'react';
import { css } from '@emotion/react';

import { Todo } from '@demo-react-query/features/todos/types';
import { TodoListItem, TodoListItemProps } from '../todo-list-item/TodoListItem';

export type TodoListProps = {
  todos: Todo[];
  isDisabled: boolean;
  onChangeIsDone: TodoListItemProps['onChangeIsDone'];
  onRemove: TodoListItemProps['onRemove'];
};

export const TodoList = memo(({
  todos,
  isDisabled,
  onChangeIsDone,
  onRemove,
}: TodoListProps) => (
  <ul
    css={css`
      list-style-type: none;
    `}
  >
    {todos.map((todo) => (
      <TodoListItem
        key={todo.id}
        todo={todo}
        isDisabled={isDisabled}
        onChangeIsDone={onChangeIsDone}
        onRemove={onRemove}
      />
    ))}
  </ul>
));
