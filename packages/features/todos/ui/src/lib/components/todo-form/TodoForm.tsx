import {
  FormEventHandler,
  memo,
  useCallback,
  useRef,
  useState,
} from 'react';
import { css } from '@emotion/react';

export type TodoFormProps = {
  onSubmit: (label: string) => Promise<void>;
  isDisabled: boolean;
};

export const TodoForm = memo(({
  onSubmit,
  isDisabled,
}: TodoFormProps) => {
  const [label, onChangeLabel] = useState<string>('');
  const labelRef = useRef<string>(label);

  labelRef.current = label;

  const onSubmitProxy: FormEventHandler<HTMLFormElement> = useCallback(
    async (event) => {
      event.preventDefault();
      await onSubmit(labelRef.current);
      onChangeLabel('');
    },
    [onSubmit],
  );

  const onChangeProxy: FormEventHandler<HTMLInputElement> = useCallback(
    (event) => onChangeLabel((event.target as HTMLInputElement).value),
    [],
  );

  return (
    <form
      onSubmit={onSubmitProxy}
      css={css`
        display: flex;
        align-items: center;
        gap: 10px;
      `}
    >
      <input
        value={label}
        onChange={onChangeProxy}
        disabled={isDisabled}
      />

      <button
        type="submit"
        disabled={isDisabled}
      >
        Add
      </button>
    </form>
  );
});
