import { FormEventHandler, memo, useCallback } from 'react';
import { css } from '@emotion/react';

import { Todo } from '@demo-react-query/features/todos/types';

export type TodoListItemProps = {
  todo: Todo;
  isDisabled: boolean;
  onChangeIsDone: (todoId: string, isDone: boolean) => void;
  onRemove: (todoId: string) => void;
};

export const TodoListItem = memo(({
  todo,
  isDisabled,
  onChangeIsDone,
  onRemove,
}: TodoListItemProps) => {
  const onChangeProxy: FormEventHandler<HTMLInputElement> = useCallback(
    (event) => onChangeIsDone(todo.id, (event.target as HTMLInputElement).checked),
    [onChangeIsDone, todo.id],
  );

  const onRemoveProxy = useCallback(
    () => onRemove(todo.id),
    [onRemove, todo.id],
  );

  return (
    <li
      css={css`
        display: flex;
        align-items: center;
        gap: 10px;
      `}
    >
      <input
        type="checkbox"
        onChange={onChangeProxy}
        checked={todo.isDone}
        disabled={isDisabled}
      />

      {todo.label}

      <button type="button" onClick={onRemoveProxy}>Remove</button>
    </li>
  );
})
