import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  TodoForm,
  TodoFormProps,
  TodoList,
  TodoListProps,
} from '@demo-react-query/features/todos/ui';

import { AppDispatch } from '../redux/types';
import { createTodo, deleteTodo, listAllTodos, updateTodo } from '../redux/todos/actions';
import {
  selectIsCreateTodoLoading,
  selectIsDeleteTodoLoading,
  selectIsUpdateTodoLoading,
  selectTodosList,
} from '../redux/todos/selectors';

export function App() {
  const todos = useSelector(selectTodosList);
  const isUpdateLoading = useSelector(selectIsUpdateTodoLoading);
  const isCreateLoading = useSelector(selectIsCreateTodoLoading);
  const isRemoveLoading = useSelector(selectIsDeleteTodoLoading);
  const dispatch: AppDispatch = useDispatch();

  useEffect(
    () => { dispatch(listAllTodos()); },
    [dispatch],
  );

  const onChangeIsDone: TodoListProps['onChangeIsDone'] = useCallback(
    (todoId, isDone) => {
      dispatch(updateTodo(todoId, { isDone }));
    },
    [dispatch],
  );

  const onRemove: TodoListProps['onRemove'] = useCallback(
    (todoId) => { dispatch(deleteTodo(todoId)); },
    [dispatch],
  );

  const onCreateTodo: TodoFormProps['onSubmit'] = useCallback(
    async (label) => { await dispatch(createTodo({ label })) },
    [dispatch],
  );

  return (
    <>
      <TodoList
        todos={todos}
        onChangeIsDone={onChangeIsDone}
        isDisabled={isUpdateLoading || isRemoveLoading}
        onRemove={onRemove}
      />

      <TodoForm
        onSubmit={onCreateTodo}
        isDisabled={isCreateLoading}
      />
    </>
  );
}
