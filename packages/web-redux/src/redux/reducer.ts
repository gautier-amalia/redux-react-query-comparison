import { combineReducers } from 'redux';

import { todosReducer } from "./todos/reducers";

export const reducer = combineReducers({
  todos: todosReducer,
});
