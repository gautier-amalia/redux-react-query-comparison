import { ThunkDispatch } from 'redux-thunk';
import { TodosAction, TodosState } from './todos/types';

export type AppState = {
  todos: TodosState;
}

export type AppAction = TodosAction;

export type AppDispatch = ThunkDispatch<AppState, never, AppAction>;
