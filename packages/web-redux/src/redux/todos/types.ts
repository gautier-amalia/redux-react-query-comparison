import { Todo } from '@demo-react-query/features/todos/types';

import {
  CREATE_TODO_FAILURE,
  CREATE_TODO_LOADING,
  CREATE_TODO_SUCCESS,
  DELETE_TODO_FAILURE,
  DELETE_TODO_LOADING,
  DELETE_TODO_SUCCESS,
  LIST_ALL_TODOS_FAILURE,
  LIST_ALL_TODOS_LOADING,
  LIST_ALL_TODOS_SUCCESS,
  UPDATE_TODO_FAILURE,
  UPDATE_TODO_LOADING,
  UPDATE_TODO_SUCCESS,
} from './constants';

export type TodosState = {
  list: Todo[];

  listAllLoading: number;
  listAllError: Error | null;

  createLoading: number;
  createError: Error | null;

  updateLoading: number;
  updateError: Error | null;

  deleteLoading: number;
  deleteError: Error | null;
};

export type ListAllTodosLoadingAction = {
  type: typeof LIST_ALL_TODOS_LOADING;
};

export type ListAllTodosSuccessAction = {
  type: typeof LIST_ALL_TODOS_SUCCESS;
  payload: {
    todos: Todo[];
  };
};

export type ListAllTodosFailureAction = {
  type: typeof LIST_ALL_TODOS_FAILURE;
  payload: {
    error: Error;
  };
};

export type CreateTodoLoadingAction = {
  type: typeof CREATE_TODO_LOADING;
};

export type CreateTodoSuccessAction = {
  type: typeof CREATE_TODO_SUCCESS;
  payload: {
    todo: Todo;
  };
};

export type CreateTodoFailureAction = {
  type: typeof CREATE_TODO_FAILURE;
  payload: {
    error: Error;
  };
};

export type UpdateTodoLoadingAction = {
  type: typeof UPDATE_TODO_LOADING;
};

export type UpdateTodoSuccessAction = {
  type: typeof UPDATE_TODO_SUCCESS;
  payload: {
    todo: Todo;
  };
};

export type UpdateTodoFailureAction = {
  type: typeof UPDATE_TODO_FAILURE;
  payload: {
    error: Error;
  };
};

export type DeleteTodoLoadingAction = {
  type: typeof DELETE_TODO_LOADING;
};

export type DeleteTodoSuccessAction = {
  type: typeof DELETE_TODO_SUCCESS;
  payload: {
    todoId: string;
  };
};

export type DeleteTodoFailureAction = {
  type: typeof DELETE_TODO_FAILURE;
  payload: {
    error: Error;
  };
};

export type TodosAction = (
  ListAllTodosLoadingAction
  | ListAllTodosSuccessAction
  | ListAllTodosFailureAction
  | CreateTodoLoadingAction
  | CreateTodoSuccessAction
  | CreateTodoFailureAction
  | UpdateTodoLoadingAction
  | UpdateTodoSuccessAction
  | UpdateTodoFailureAction
  | DeleteTodoLoadingAction
  | DeleteTodoSuccessAction
  | DeleteTodoFailureAction
);
