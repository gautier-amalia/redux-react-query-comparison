import { AppState } from '../types';

export const selectTodosList = (state: AppState) => state.todos.list;
export const selectIsListTodosLoading = (state: AppState) => state.todos.listAllLoading > 0;
export const selectListTodosError = (state: AppState) => state.todos.listAllError;
export const selectIsCreateTodoLoading = (state: AppState) => state.todos.createLoading > 0;
export const selectCreateTodoError = (state: AppState) => state.todos.createError;
export const selectIsUpdateTodoLoading = (state: AppState) => state.todos.updateLoading > 0;
export const selectUpdateTodoError = (state: AppState) => state.todos.updateError;
export const selectIsDeleteTodoLoading = (state: AppState) => state.todos.deleteLoading > 0;
export const selectDeleteTodoError = (state: AppState) => state.todos.deleteError;
