import { combineReducers } from 'redux';

import { AppAction, AppState } from '../types';
import {
  CREATE_TODO_FAILURE,
  CREATE_TODO_LOADING,
  CREATE_TODO_SUCCESS,
  DELETE_TODO_FAILURE,
  DELETE_TODO_LOADING,
  DELETE_TODO_SUCCESS,
  LIST_ALL_TODOS_FAILURE,
  LIST_ALL_TODOS_LOADING,
  LIST_ALL_TODOS_SUCCESS,
  UPDATE_TODO_FAILURE,
  UPDATE_TODO_LOADING,
  UPDATE_TODO_SUCCESS,
} from './constants';

export const todosReducer = combineReducers({
  list: (
    state: AppState['todos']['list'] = [],
    action: AppAction
  ): AppState['todos']['list'] => {
    switch (action.type) {
      case LIST_ALL_TODOS_SUCCESS:
        return action.payload.todos;

      case CREATE_TODO_SUCCESS:
        return [...state, action.payload.todo];

      case UPDATE_TODO_SUCCESS:
        return state.map((todo) => (
          todo.id === action.payload.todo.id
            ? action.payload.todo
            : todo
        ));

      case DELETE_TODO_SUCCESS:
        return state.filter(({ id }) => id !== action.payload.todoId);

      default:
        return state;
    }
  },

  listAllLoading: (
    state: AppState['todos']['listAllLoading'] = 0,
    action: AppAction,
  ): AppState['todos']['listAllLoading'] => {
    switch (action.type) {
      case LIST_ALL_TODOS_LOADING:
        return state + 1;

      case LIST_ALL_TODOS_SUCCESS:
      case LIST_ALL_TODOS_FAILURE:
        return state - 1;

      default:
        return state;
    }
  },

  listAllError: (
    state: AppState['todos']['listAllError'] = null,
    action: AppAction,
  ): AppState['todos']['listAllError'] => {
    switch (action.type) {
      case LIST_ALL_TODOS_LOADING:
        return null;

      case LIST_ALL_TODOS_FAILURE:
        return action.payload.error;

      default:
        return state;
    }
  },

  createLoading: (
    state: AppState['todos']['createLoading'] = 0,
    action: AppAction,
  ): AppState['todos']['createLoading'] => {
    switch (action.type) {
      case CREATE_TODO_LOADING:
        return state + 1;

      case CREATE_TODO_SUCCESS:
      case CREATE_TODO_FAILURE:
        return state - 1;

      default:
        return state;
    }
  },

  createError: (
    state: AppState['todos']['createError'] = null,
    action: AppAction,
  ): AppState['todos']['createError'] => {
    switch (action.type) {
      case CREATE_TODO_LOADING:
        return null;

      case CREATE_TODO_FAILURE:
        return action.payload.error;

      default:
        return state;
    }
  },

  updateLoading: (
    state: AppState['todos']['updateLoading'] = 0,
    action: AppAction,
  ): AppState['todos']['updateLoading'] => {
    switch (action.type) {
      case UPDATE_TODO_LOADING:
        return state + 1;

      case UPDATE_TODO_SUCCESS:
      case UPDATE_TODO_FAILURE:
        return state - 1;

      default:
        return state;
    }
  },

  updateError: (
    state: AppState['todos']['updateError'] = null,
    action: AppAction,
  ): AppState['todos']['updateError'] => {
    switch (action.type) {
      case UPDATE_TODO_LOADING:
        return null;

      case UPDATE_TODO_FAILURE:
        return action.payload.error;

      default:
        return state;
    }
  },

  deleteLoading: (
    state: AppState['todos']['deleteLoading'] = 0,
    action: AppAction,
  ): AppState['todos']['deleteLoading'] => {
    switch (action.type) {
      case DELETE_TODO_LOADING:
        return state + 1;

      case DELETE_TODO_SUCCESS:
      case DELETE_TODO_FAILURE:
        return state - 1;

      default:
        return state;
    }
  },

  deleteError: (
    state: AppState['todos']['deleteError'] = null,
    action: AppAction,
  ): AppState['todos']['deleteError'] => {
    switch (action.type) {
      case DELETE_TODO_LOADING:
        return null;

      case DELETE_TODO_FAILURE:
        return action.payload.error;

      default:
        return state;
    }
  },
});
