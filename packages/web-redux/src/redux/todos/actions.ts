import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { PatchTodoDto, PostTodoDto, Todo } from '@demo-react-query/features/todos/types';
import { TodoRepository } from '@demo-react-query/features/todos/repository';

import { AppState } from '../types';
import {
  CreateTodoFailureAction,
  CreateTodoLoadingAction,
  CreateTodoSuccessAction,
  DeleteTodoFailureAction,
  DeleteTodoLoadingAction,
  DeleteTodoSuccessAction,
  ListAllTodosFailureAction,
  ListAllTodosLoadingAction,
  ListAllTodosSuccessAction,
  UpdateTodoFailureAction,
  UpdateTodoLoadingAction,
  UpdateTodoSuccessAction,
} from './types';
import {
  CREATE_TODO_FAILURE,
  CREATE_TODO_LOADING,
  CREATE_TODO_SUCCESS,
  DELETE_TODO_FAILURE,
  DELETE_TODO_LOADING,
  DELETE_TODO_SUCCESS,
  LIST_ALL_TODOS_FAILURE,
  LIST_ALL_TODOS_LOADING,
  LIST_ALL_TODOS_SUCCESS,
  UPDATE_TODO_FAILURE,
  UPDATE_TODO_LOADING,
  UPDATE_TODO_SUCCESS,
} from './constants';

const todoRepository = new TodoRepository();

export const listAllTodosLoading = (): ListAllTodosLoadingAction => ({
  type: LIST_ALL_TODOS_LOADING,
});

export const listAllTodosSuccess = (todos: Todo[]): ListAllTodosSuccessAction => ({
  type: LIST_ALL_TODOS_SUCCESS,
  payload: {
    todos,
  },
});

export const listAllTodosFailure = (error: Error): ListAllTodosFailureAction => ({
  type: LIST_ALL_TODOS_FAILURE,
  payload: {
    error,
  },
});

export const listAllTodos = (): ThunkAction<
  Promise<ListAllTodosSuccessAction | ListAllTodosFailureAction>,
  AppState,
  never,
  Action
> => async (dispatch) => {
  dispatch(listAllTodosLoading());

  try {
    const todos = await todoRepository.listAll();
    return dispatch(listAllTodosSuccess(todos));
  } catch (err) {
    return dispatch(listAllTodosFailure(err as Error));
  }
};

export const createTodoLoading = (): CreateTodoLoadingAction => ({
  type: CREATE_TODO_LOADING,
});

export const createTodoSuccess = (todo: Todo): CreateTodoSuccessAction => ({
  type: CREATE_TODO_SUCCESS,
  payload: {
    todo,
  },
});

export const createTodoFailure = (error: Error): CreateTodoFailureAction => ({
  type: CREATE_TODO_FAILURE,
  payload: {
    error,
  },
});

export const createTodo = (postTodoDto: PostTodoDto): ThunkAction<
  Promise<CreateTodoSuccessAction | CreateTodoFailureAction>,
  AppState,
  never,
  Action
> => async (dispatch) => {
  dispatch(createTodoLoading());

  try {
    const todo = await todoRepository.create(postTodoDto);
    return dispatch(createTodoSuccess(todo));
  } catch (err) {
    return dispatch(createTodoFailure(err as Error));
  }
};

export const updateTodoLoading = (): UpdateTodoLoadingAction => ({
  type: UPDATE_TODO_LOADING,
});

export const updateTodoSuccess = (todo: Todo): UpdateTodoSuccessAction => ({
  type: UPDATE_TODO_SUCCESS,
  payload: {
    todo,
  },
});

export const updateTodoFailure = (error: Error): UpdateTodoFailureAction => ({
  type: UPDATE_TODO_FAILURE,
  payload: {
    error,
  },
});

export const updateTodo = (
  todoId: string,
  patchTodoDto: PatchTodoDto,
): ThunkAction<
  Promise<UpdateTodoSuccessAction | UpdateTodoFailureAction>,
  AppState,
  never,
  Action
> => async (dispatch) => {
  dispatch(updateTodoLoading());

  try {
    const todo = await todoRepository.updateOne(todoId, patchTodoDto);
    return dispatch(updateTodoSuccess(todo));
  } catch (err) {
    return dispatch(updateTodoFailure(err as Error));
  }
};

export const deleteTodoLoading = (): DeleteTodoLoadingAction => ({
  type: DELETE_TODO_LOADING,
});

export const deleteTodoSuccess = (todoId: string): DeleteTodoSuccessAction => ({
  type: DELETE_TODO_SUCCESS,
  payload: {
    todoId,
  },
});

export const deleteTodoFailure = (error: Error): DeleteTodoFailureAction => ({
  type: DELETE_TODO_FAILURE,
  payload: {
    error,
  },
});

export const deleteTodo = (
  todoId: string,
): ThunkAction<
  Promise<DeleteTodoSuccessAction | DeleteTodoFailureAction>,
  AppState,
  never,
  Action
> => async (dispatch) => {
  dispatch(deleteTodoLoading());

  try {
    await todoRepository.deleteOne(todoId);
    return dispatch(deleteTodoSuccess(todoId));
  } catch (err) {
    return dispatch(deleteTodoFailure(err as Error));
  }
};
