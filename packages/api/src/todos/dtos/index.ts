import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export class PostTodoDto {
  @ApiProperty()
  label: string;

  @ApiPropertyOptional({ default: false })
  isDone?: boolean;
}

export class PatchTodoDto {
  @ApiPropertyOptional()
  label?: string;

  @ApiPropertyOptional()
  isDone?: boolean;
}
