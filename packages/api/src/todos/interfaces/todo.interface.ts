import { ApiProperty } from "@nestjs/swagger";

export class Todo {
  @ApiProperty()
  id: string;

  @ApiProperty()
  label: string;

  @ApiProperty()
  isDone: boolean;
}
