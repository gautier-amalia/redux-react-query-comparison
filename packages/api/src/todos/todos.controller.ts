import { Body, Controller, Get, NotFoundException, Param, Post, Patch, Delete } from '@nestjs/common';
import { ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse } from '@nestjs/swagger';

import { PostTodoDto, PatchTodoDto } from './dtos';
import { Todo } from './interfaces/todo.interface';
import { TodosService } from './todos.service';

const sleep = (delayInMs = 200) => new Promise((resolve) => { setTimeout(resolve, delayInMs); });

@Controller('todos')
export class TodosController {
  constructor(private todosService: TodosService) {}

  @Get()
  @ApiOkResponse({ type: [Todo] })
  async listAll(): Promise<Todo[]> {
    await sleep();
    return this.todosService.listAll();
  }

  @Post()
  @ApiCreatedResponse({ type: Todo })
  async create(@Body() body: PostTodoDto): Promise<Todo> {
    await sleep();
    return this.todosService.create(body);
  }

  @Patch(':id')
  @ApiOkResponse({ type: Todo })
  @ApiNotFoundResponse()
  async updateOne(
    @Param('id') id: string,
    @Body() body: PatchTodoDto,
  ): Promise<Todo> {
    await sleep();

    const todo = this.todosService.update(id, body);

    if (!todo) {
      throw new NotFoundException(`Could not find todo ${id}`);
    }

    return todo;
  }

  @Delete(':id')
  @ApiOkResponse()
  @ApiNotFoundResponse()
  async deleteOne(@Param('id') id: string) {
    await sleep();

    if (!this.todosService.delete(id)) {
      throw new NotFoundException(`Could not find todo ${id}`);
    }
  }
}
