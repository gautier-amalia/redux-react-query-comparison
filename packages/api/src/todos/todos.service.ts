import { uniqueId } from 'lodash';
import { Injectable } from '@nestjs/common';

import { Todo } from './interfaces/todo.interface';
import { PatchTodoDto, PostTodoDto } from './dtos';

@Injectable()
export class TodosService {
  private todos: Todo[] = [];

  listAll() {
    return this.todos;
  }

  create({
    label,
    isDone,
  }: PostTodoDto): Todo {
    const todo = {
      id: uniqueId('todo'),
      isDone: !!isDone,
      label,
    };

    this.todos.push(todo);
    return todo;
  }

  update(
    id: string,
    {
      label,
      isDone,
    }: PatchTodoDto
  ): Todo {
    const todo = this.todos.find((t) => t.id === id);
    if (!todo) { return null; }

    todo.label = label ?? todo.label;
    todo.isDone = isDone ?? todo.isDone;
    return todo;
  }

  delete(
    id: string,
  ): boolean {
    if (!this.todos.find((t) => t.id === id)) { return false; }
    this.todos = this.todos.filter((todo) => todo.id !== id);
    return true;
  }
}
