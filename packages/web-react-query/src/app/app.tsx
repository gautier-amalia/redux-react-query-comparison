import { useCallback } from 'react';

import { TodoForm, TodoFormProps, TodoList, TodoListProps } from '@demo-react-query/features/todos/ui';

import { useTodos } from '../state/todos/queries';
import { useCreateTodo, useDeleteTodo, useUpdateTodo } from '../state/todos/mutations';

export function App() {
  const { data: todos } = useTodos();
  const { mutateAsync: onCreateTodo, isLoading: isCreateLoading } = useCreateTodo();
  const { mutateAsync: onUpdateTodo, isLoading: isUpdateLoading } = useUpdateTodo();
  const { mutateAsync: onRemoveTodo, isLoading: isRemoveLoading } = useDeleteTodo();

  const onChangeIsDone: TodoListProps['onChangeIsDone'] = useCallback(
    (todoId, isDone) => {
      onUpdateTodo({ id: todoId, payload: { isDone } });
    },
    [onUpdateTodo],
  );

  const onSubmitForm: TodoFormProps['onSubmit'] = useCallback(
    async (label) => { await onCreateTodo({ label })},
    [onCreateTodo],
  );

  return (
    todos
      ? (
        <>
          <TodoList
            todos={todos}
            onChangeIsDone={onChangeIsDone}
            isDisabled={isUpdateLoading || isRemoveLoading}
            onRemove={onRemoveTodo}
          />

          <TodoForm
            onSubmit={onSubmitForm}
            isDisabled={isCreateLoading}
          />
        </>
      )
      : null
  );
}
