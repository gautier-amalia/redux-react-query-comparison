import { useQuery } from '@tanstack/react-query';
import { TodoRepository } from '@demo-react-query/features/todos/repository';

import { TODOS_QUERY_KEY } from './constants';

const todoRepository = new TodoRepository();

export const useTodos = () => useQuery({
  queryKey: [TODOS_QUERY_KEY],
  queryFn: () => todoRepository.listAll(),
});
