import { useMutation, useQueryClient } from '@tanstack/react-query';

import { PatchTodoDto, PostTodoDto } from '@demo-react-query/features/todos/types';
import { TodoRepository } from '@demo-react-query/features/todos/repository';
import { TODOS_QUERY_KEY } from './constants';

const todoRepository = new TodoRepository();

export const useCreateTodo = () => {
  const queryClient = useQueryClient();

  return useMutation(
    (postTodoDto: PostTodoDto) => todoRepository.create(postTodoDto),
    {
      onSuccess: () => {
        queryClient.invalidateQueries([TODOS_QUERY_KEY]);
      },
    },
  );
};

export const useUpdateTodo = () => {
  const queryClient = useQueryClient();

  return useMutation(
    ({
      id,
      payload,
    }: {
      id: string;
      payload: PatchTodoDto;
    }) => todoRepository.updateOne(id, payload),
    {
      onSuccess: () => {
        queryClient.invalidateQueries([TODOS_QUERY_KEY]);
      },
    },
  );
};

export const useDeleteTodo = () => {
  const queryClient = useQueryClient();

  return useMutation(
    (id: string) => todoRepository.deleteOne(id),
    {
      onSuccess: () => {
        queryClient.invalidateQueries([TODOS_QUERY_KEY]);
      },
    },
  );
};
